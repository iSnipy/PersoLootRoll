## Interface: 70100
## Title: PersoLootRoll
## Notes: A loot roll addon for personal loot.
## Version: 1
## SavedVariables: PersoLootRollDB, PersoLootRollIconDB, PLR_MASTERLOOTER

# Libs
Libs\libs.xml

# Initialization
Init.lua
Util\Util.lua

# Util, Locale
Util\Util.lua
Util\Unit.lua
Util\Locale.lua
Locale\enUS.lua
Locale\deDE.lua
Util\Comm.lua
Util\Item.lua

# Data
Data\Realms.lua
Data\Trinkets.lua

# Application logic
Addon.lua
Masterloot.lua
Roll.lua
Trade.lua
Inspect.lua
GUI.xml
Events.lua
Hooks.lua