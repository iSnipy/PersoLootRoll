local Name, Addon = ...
local Locale = Addon.Locale
local lang = "enUS"

-- Chat messages
local L = {}
setmetatable(L, Locale.MT)
Locale[lang] = L

L["ROLL_START"] = Addon.PREFIX_CHAT .. " Giving away %s -> /w me or /roll %d!"
L["ROLL_START_MASTERLOOT"] = Addon.PREFIX_CHAT .. " Giving away %s from <%s> -> /w me or /roll %d!"
L["ROLL_WINNER"] = Addon.PREFIX_CHAT .. " <%s> has won %s -> Trade me!"
L["ROLL_WINNER_MASTERLOOT"] = Addon.PREFIX_CHAT .. " <%s> has won %s from <%s> -> Trade %s!"
L["ROLL_WINNER_WHISPER"] = "You have won %s! Please trade me."
L["ROLL_WINNER_WHISPER_MASTERLOOT"] = "You have won %s from <%s>! Please trade %s."
L["ROLL_ANSWER_BID"] = "Ok, I registered your bid for %s."
L["ROLL_ANSWER_YES"] = "You can have it, just trade me."
L["ROLL_ANSWER_YES_MASTERLOOT"] = "You can have it, just trade <%s>."
L["ROLL_ANSWER_NO_SELF"] = "Sorry, I need that myself."
L["ROLL_ANSWER_NO_OTHER"] = "Sorry, I already gave it to someone else."
L["ROLL_ANSWER_NOT_TRADABLE"] = "Sorry, I can't trade it."
L["ROLL_ANSWER_AMBIGUOUS"] = "I am giving away multiple items right now, please send me the link of the item you want."
L["BID"] = "Do you need that %s?"
L["ITEM"] = "item"
L["HER"] = "her"
L["HIM"] = "him"

-- Addon
local L = LibStub("AceLocale-3.0"):NewLocale(Name, lang, lang == Locale.DEFAULT)
if not L then return end

LOOT_ROLL_INELIGIBLE_REASONPLR_NO_ADDON = "The owner of this item doesn't use the PersoLootRoll addon."
LOOT_ROLL_INELIGIBLE_REASONPLR_NO_DISENCHANT = "The PersoLootRoll addon doesn't support disenchanting."

L["ENABLED"] = "Enabled"
L["DISABLED"] = "Disabled"
L["ITEM"] = "item"
L["ID"] = ID
L["OWNER"] = "Owner"
L["STATUS"] = STATUS
L["WINNER"] = "Winner"
L["YOUR_BID"] = "Your bid"
L["PLAYER"] = "Player"
L["LEVEL"] = LEVEL
L["ITEM_LEVEL"] = "Item-Level"
L["BID"] = "Bid"
L["VOTE"] = "Vote"
L["VOTE_WITHDRAW"] = "Withdraw"
L["VOTES"] = "Votes"
L["ACTIONS"] = "Actions"
L["AWARD"] = "Award"
L["AWARD_LOOT"] = "Award loot"
L["AWARD_RANDOMLY"] = "Award randomly"
L["ROLLS"] = "Rolls"
L["ADVERTISE"] = "Advertise in chat"
L["GUILD_MASTER"] = "Guild Master"
L["GUILD_OFFICER"] = "Guild Officer"
L["RAID_LEADER"] = "Raid leader"
L["RAID_ASSISTANT"] = "Raid assistant"
L["MASTERLOOTER"] = "Masterlooter"
L["ML"] = "ML"
L["PUBLIC"] = "Public"
L["PRIVATE"] = "Private"
L["RESTART"] = "Restart"

L["HELP"] = "Start rolls and bid for items (/PersoLootRoll or /plr).\n"
 .. "Usage: \n"
 .. "/plr: Open options window\n"
 .. "/plr roll [item]* (<timeout> <owner>): Start a roll for one or more item(s)\n"
 .. "/plr bid <owner> ([item]): Bid for an item from another player\n"
 .. "/plr options: Open options window\n"
 .. "/plr config: Change settings through the command line\n"
 .. "/plr help: Print this help message\n"
 .. "Legend: [..] = item link, * = one or more times, (..) = optional"
L["USAGE_ROLL"] = "Usage: /plr roll [item]* (<timeout> <owner>)"
L["USAGE_BID"] = "Usage: /plr bid <owner> ([item])"

L["VERSION_NOTICE"] = "There's a new version of this addon available. Please update to stay compatible with everyone and not miss out on any loot!"

L["ROLL_START"] = "Starting roll for %s from %s."
L["ROLL_END"] = "Ending roll for %s from %s."
L["ROLL_WINNER_OWN"] = "You have won your own %s."
L["ROLL_WINNER_SELF"] = "You have won %s from %s -> %s."
L["ROLL_WINNER_OTHER"] = "%s has won %s from you -> %s."
L["ROLL_WINNER_MASTERLOOT"] = "%s has won %s from %s."
L["ROLL_CANCEL"] = "Canceling roll for %s from %s."
L["ROLL_BID_1"] = NEED
L["ROLL_BID_2"] = GREED
L["ROLL_BID_3"] = ROLL_DISENCHANT
L["ROLL_BID_4"] = PASS
L["ROLL_STATUS_-1"] = "Canceled"
L["ROLL_STATUS_0"] = "Pending"
L["ROLL_STATUS_1"] = "Running"
L["ROLL_STATUS_2"] = "Done"
L["ROLL_AWARDED"] = "Awarded"
L["ROLL_TRADED"] = "Traded"

L["BID_START"] = "Bidding with %q for %s from %s."
L["BID_PASS"] = "Passing on %s from %s."
L["BID_CHAT"] = "Asking %s for %s -> %s."
L["BID_NO_CHAT"] = "Whispering is disabled, you need to ask %s for %s yourself -> %s."

L["TRADE_START"] = "Starting trade with %s."
L["TRADE_CANCEL"] = "Canceling trade with %s."

L["MASTERLOOTER_SELF"] = "You are now the masterlooter."
L["MASTERLOOTER_OTHER"] = "%s is now your masterlooter."

L["FILTER"] = "Filter"
L["FILTER_ALL"] = "For all players"
L["FILTER_ALL_DESC"] = "Include rolls for all players, not just yours or those with items that might interest you."
L["FILTER_CANCELED"] = "Canceled"
L["FILTER_CANCELED_DESC"] = "Include canceled rolls."
L["FILTER_DONE"] = "Done"
L["FILTER_DONE_DESC"] = "Include rolls that have ended."
L["FILTER_AWARDED"] = "Awarded"
L["FILTER_AWARDED_DESC"] = "Include rolls that have been won by someone."
L["FILTER_TRADED"] = "Traded"
L["FILTER_TRADED_DESC"] = "Include rolls whose items have been traded."

L["TIP_MINIMAP_ICON"] = "|cffffff00Left-Click:|r Toggle rolls window\n|cffffff00Right-Click:|r Show Options"
L["TIP_MASTERLOOT_START"] = "Become or search for a masterlooter"
L["TIP_MASTERLOOT_STOP"] = "Remove masterlooter"
L["TIP_MASTERLOOT"] = "Masterloot is active"
L["TIP_MASTERLOOT_INFO"] = "|cffffff00Masterlooter:|r %s\n|cffffff00Council:|r %s\n|cffffff00Bids:|r %s\n|cffffff00Votes:|r %s"
L["TIP_MASTERLOOTING"] = "You are the masterlooter for:"

L["MENU_MASTERLOOT_START"] = "Become masterlooter"
L["MENU_MASTERLOOT_SEARCH"] = "Search group for a masterlooter"

L["DIALOG_ROLL_CANCEL"] = "Do you want to cancel this roll?"
L["DIALOG_ROLL_RESTART"] = "Do you want to restart this roll?"
L["DIALOG_MASTERLOOT_ASK"] = "<%s> wants to become your masterlooter, do you want to accept that?"

L["ERROR_CMD_UNKNOWN"] = "Unknown command '%s'"
L["ERROR_PLAYER_NOT_FOUND"] = "Cannot find player %s."
L["ERROR_ITEM_NOT_TRADABLE"] = "You cannot trade that item."
L["ERROR_NOT_IN_GROUP"] = "You are not in a group or raid."
L["ERROR_ROLL_UNKNOWN"] = "That roll doesn't exist."
L["ERROR_ROLL_STATUS_NOT_0"] = "The roll has already been started or finished."
L["ERROR_ROLL_STATUS_NOT_1"] = "The roll is not running."
L["ERROR_ROLL_BID_UNKNOWN_SELF"] = "That's not a valid bid."
L["ERROR_ROLL_BID_UNKNOWN_OTHER"] = "%s has send an invalid bid for %s."

L["OPT_GENERAL"] = "General"
L["OPT_MESSAGES"] = "Messages"
L["OPT_LOOT_METHOD"] = "Loot Method"

L["OPT_ENABLE"] = "Enable"
L["OPT_ENABLE_DESC"] = "Enable or disable the addon"
L["OPT_INFO"] = "Information"
L["OPT_INFO_DESC"] = "Some information about this addon."
L["OPT_VERSION"] = "|cffffff00Version:|r " .. Addon.VERSION
L["OPT_AUTHOR"] = "|cffffff00Author:|r Shrugal-Mal'Ganis (EU)"
L["OPT_TRANSLATION"] = "|cffffff00Translation:|r Shrugal-Mal'Ganis (EU)"
L["OPT_UI"] = "User interface"
L["OPT_UI_DESC"] = "Customize " .. Name .. "'s look and feel to your liking."
L["OPT_MINIMAP_ICON"] = "Show minimap icon"
L["OPT_MINIMAP_ICON_DESC"] = "Show or hide the minimap icon"
L["OPT_ROLL_FRAMES"] = "Show roll frames"
L["OPT_ROLL_FRAMES_DESC"] = "Show the roll frames when someone loots something you might be interested in, so you can roll for it."
L["OPT_ROLLS_WINDOW"] = "Show rolls window"
L["OPT_ROLLS_WINDOW_DESC"] = "Always show the rolls window (with all rolls on it) when someone loots something you might be interested in. This is always enabled when you are a masterlooter."

L["OPT_ECHO"] = "Chat information"
L["OPT_ECHO_DESC"] = "How much information do you want to see from the addon in chat?\n\n"
    .. "|cffffff00None:|r No info in chat.\n"
    .. "|cffffff00Error:|r Only error messages.\n"
    .. "|cffffff00Info:|r Errors and useful info that you probably want to act on.\n"
    .. "|cffffff00Verbose:|r Get notices about pretty much anything the addon does.\n"
    .. "|cffffff00Debug:|r Same as verbose, plus additional debug info."
L["OPT_ECHO_NONE"] = "None"
L["OPT_ECHO_ERROR"] = "Error"
L["OPT_ECHO_INFO"] = "Info"
L["OPT_ECHO_VERBOSE"] = "Verbose"
L["OPT_ECHO_DEBUG"] = "Debug"
L["OPT_GROUPCHAT"] = "Group Chat"
L["OPT_GROUPCHAT_DESC"] = "Change whether or not the addon will post things to the group chat."
L["OPT_GROUPCHAT_ANNOUNCE"] = "Announce rolls and winners"
L["OPT_GROUPCHAT_ANNOUNCE_DESC"] = "Announce rolls and winners in group/raid chat."
L["OPT_GROUPCHAT_ROLL"] = "Roll on loot in chat"
L["OPT_GROUPCHAT_ROLL_DESC"] = "Roll on loot you want (/roll) if others post links in group/raid chat."
L["OPT_WHISPER"] = "Whisper Chat"
L["OPT_WHISPER_DESC"] = "Change whether or not the addon will whisper other players and/or answer incoming messages."
L["OPT_WHISPER_ANSWER"] = "Answer to whispers"
L["OPT_WHISPER_ANSWER_DESC"] = "Let the addon answer whispers from group/raid members about items you looted."
L["OPT_WHISPER_GROUP"] = "Whisper by group type"
L["OPT_WHISPER_GROUP_DESC"] = "Whisper others if they got loot you want, depending on the type of group you are currently in."
L["OPT_WHISPER_TARGET"] = "Whisper by target"
L["OPT_WHISPER_TARGET_DESC"] = "Whisper others if they got loot you want, depending on whether the target is in your guild or on your friend list."

L["OPT_AWARD_SELF"] = "Choose winner yourself"
L["OPT_AWARD_SELF_DESC"] = "Choose for yourself who should get your loot, instead of letting the addon randomly pick someone. This is always enabled when you are a masterlooter."
L["OPT_MASTERLOOT"] = "Masterloot"
L["OPT_MASTERLOOT_DESC"] = "When you (or someone else) becomes masterlooter, all loot will be distributed by that person. You will get a notice about who's items you won or who won your items, so you can trade them to the right person."
L["OPT_MASTERLOOT_START"] = "Become masterlooter"
L["OPT_MASTERLOOT_SEARCH"] = "Search masterlooter"
L["OPT_MASTERLOOT_STOP"] = "Stop masterloot"
L["OPT_MASTERLOOT_ALLOW"] = "Allow becoming masterlooter"
L["OPT_MASTERLOOT_ALLOW_DESC"] = "Choose who can request to become your masterlooter. You will still get a popup message asking you to confirm it, so you can decline a masterlooter request when it happens.\n\n"
    .. "|cffffff00Guild Group:|r Someone from a gulid whose members make up 80% or more of the group.\n"
L["OPT_MASTERLOOT_ACCEPT"] = "Automatically accept masterlooter"
L["OPT_MASTERLOOT_ACCEPT_DESC"] = "Automatically accept masterlooter requests from these players."
L["OPT_MASTERLOOT_WHITELIST"] = "Masterlooter Whitelist"
L["OPT_MASTERLOOT_WHITELIST_DESC"] = "If the above options don't really fit a specific person, but you still want that player to be able to become your masterlooter, then enter the name here. Separate multiple names with spaces or commas."
L["OPT_MASTERLOOT_ALLOW_ALL"] = "Allow everbody"
L["OPT_MASTERLOOT_ALLOW_ALL_DESC"] = "|cffff0000WARNING:|r This will allow everybody to request becoming your masterlooter and potentially scam you into giving away your loot! Only activate it if you know what you are doing."
L["OPT_MASTERLOOTER"] = "Masterlooter"
L["OPT_MASTERLOOTER_DESC"] = "These options apply to everybody when you are the masterlooter."
L["OPT_MASTERLOOTER_BID_PUBLIC"] = "Bids public"
L["OPT_MASTERLOOTER_BID_PUBLIC_DESC"] = "You can make bids public, so everybody can see who bid on what."
L["OPT_MASTERLOOTER_COUNCIL"] = "Council"
L["OPT_MASTERLOOTER_COUNCIL_DESC"] = "Players on the council can vote on who should get the loot."
L["OPT_MASTERLOOTER_COUNCIL_WHITELIST"] = "Council whitelist"
L["OPT_MASTERLOOTER_COUNCIL_WHITELIST_DESC"] = "You can also name specific players to be on your council. Separate multiple people by spaces or commas."
L["OPT_MASTERLOOTER_VOTE_PUBLIC"] = "Council votes public"
L["OPT_MASTERLOOTER_VOTE_PUBLIC_DESC"] = "You can make council votes public, so everybody can see who has how many votes."